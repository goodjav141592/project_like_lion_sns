# SNS Project 1

## 📢 요구사항정의서

![](../image/요구사항정의서.png)

## 📢 기술 스택

- 에디터 : Intellij Ultimate
- 개발 툴 : SpringBoot 2.7.5
- 자바 : JAVA 11
- 빌드 : Gradle 6.8
- 서버 : AWS EC2
- 배포 : Docker
- 데이터베이스 : Mysql 8.0
- 필수 라이브러리
    - SpringBoot Web, MYSQL, Spring Data JPA, Lombok, Spring Security, JWT, Swagger

## 📢 기술 스택 구조

![](../image/img.png)

- 인텔리제이에서 깃랩으로 프로젝트를 push한다.
- 깃랩에서는 파이프라인을 통해 gradle 빌드와 docker 빌드를 진행한다.
- AWS EC2에서 깃랩 repository의 docker 이미지를 pull한다.
- AWS EC2에서 docker run을 통해 컨테이너를 띄우고 이미지를 실행한다.

## 📢 프로젝트 아키텍처

![](../image/img_1.png)

- 해당 프로젝트는 Layered Archiectur를 따르고 있다.

## 📢 ERD

![](../image/img_2.png)

## 📢 DI 클래스 다이어그램

![](../image/img_3.png)

- UserController는 UserService를 DI한다.
- UserService는 UserRepository와 EncoderConfig를 DI한다.

![](../image/img_4.png)

- SecurityConfig는 Authentication, JwtFilter를 DI한다.

![](../image/img_5.png)

- PostController는 PostService를 DI한다.
- PostService는 PostRepository와 UserRepository를 DI한다.

## 📎 기능명세서

| 구분 | Method | URL | Description | 참고 |
| --- | --- | --- | --- | --- |
| 회원 | POST | /api/v1/users/join | 회원가입 |  |
|  | POST | /api/v1/users/login | 로그인 | 로그인시 토큰 발행 |
| 포스트 | GET | /api/v1/posts | 포스트 목록 조회 | Page(최신순, 20개씩) |
|  | GET | /api/v1/posts/{id} | 포스트 상세 조회 |  |
|  | POST | /api/v1/posts | 포스트 작성 | 로그인 후 이용 가능 |
|  | PUT | /api/v1/posts | 포스트 수정 | 본인 또는 ADMIN 권한 필요 |
|  | DELETE | /api/v1/posts/{id} | 포스트 삭제 | 본인 또는 ADMIN 권한 필요 |

## 📎 ErrorCode

| 에러 코드 | 설명                                                                 | HTTP status |
| --- |--------------------------------------------------------------------| --- |
| DUPLICATED_USER_NAME | 중복된 아이디로 가입을 요청한 경우                                                | 409 |
| USERNAME_NOT_FOUND | 가입되지 않은 아이디로 요청한 경우                                                | 404 |
| INVALID_PASSWORD | 비밀번호가 틀린 경우                                                        | 401 |
| INVALID_TOKEN | 유효하지 않은 토큰으로 요청한 경우                                                | 401 |
| INVALID_PERMISSION | 해당 API에 대한 요청 권한이 없는 경우 올바르지 않은 헤더로 요청한 경우 게시글에 대한 수정/삭제 권한이 없는 경우 | 401 |
| POST_NOT_FOUND | 존재하지 않는 게시물을 요청한 경우                                                | 404 |
| DB_ERROR | DB와의 연결이 이상인 경우                                                    | 500 |
| INTERNAL_SERVER_ERROR | 서버에서 요청 처리 중 오류가 발생한 경우                                            | 500 |
| INVALID_VALUE | 지원하지 않는 포맷 입력                                                      | 400 |

## ✏️ 미션 수행

- AWS EC2 SWAGGER URL

  [**http://ec2-15-165-160-129.ap-northeast-2.compute.amazonaws.com:8080/swagger-ui/#/**](http://ec2-15-165-160-129.ap-northeast-2.compute.amazonaws.com:8080/swagger-ui/#/)


### 1️⃣ 회원가입 : Post /api/v1/users/join

- 회원가입 성공 시 `userId` , `userName` 을 반환한다.
- 실행 결과
    - 성공

    ```json
    {
      "resultCode": "SUCCESS",
      "result": {
        "userId": 3,
        "userName": "홍길동2"
      }
    }
    ```

    - 실패 (userName 중복인 경우)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "UserName이 중복됩니다.",
        "errorCode": "DUPLICATED_USER_NAME"
      }
    }
    ```


### 2️⃣ 로그인 : Post /api/v1/users/login

- 로그인 성공 시 `jwt` 를 반환한다.
- 실행결과
    - 성공

    ```json
    {
      "resultCode": "SUCCESS",
      "result": {
        "jwt": "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6Iu2Zjeq4uOuPmTEiLCJpYXQiOjE2NzIxMjcwMjgsImV4cCI6MTY3MjEzMDYyOH0.gQMHyIa-sI0f2tagE12OqfXAKk7rnMVnLz14w7AmGsY"
      }
    }
    ```

    - 실패 (패스워드가 다른 경우)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "패스워드가 다릅니다.",
        "errorCode": "INVALID_PASSWORD"
      }
    }
    ```

    - 실패(userName이 존재하지 않는 경우)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "존재하지 않는 UserName입니다.",
        "errorCode": "USERNAME_NOT_FOUND"
      }
    }
    ```


### 3️⃣ 포스트 작성 : Post /api/v1/posts

- 포스트 작성 성공 시 `message` , `postId` 를 반환한다.
- 실행결과
    - 성공

    ```json
    {
      "resultCode": "SUCCESS",
      "result": {
        "message": "포스트 등록 완료",
        "postId": 20
      }
    }
    ```

    - 실패 (토큰 인증 실패)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "잘못된 토큰입니다.",
        "errorCode": "UNAUTHORIZED"
      }
    }
    ```


### 4️⃣ 포스트 상세 조회 : Get /api/v1/posts/{id}

- 포스트 상세 조회 성공 시 `id` , `title`, `body`, `userName`, `createAt`, `lastModifiedAt` 을 반환한다.
- 실행결과
    - 성공

    ```json
    {
      "resultCode": "SUCCESS",
      "result": {
        "id": 3,
        "title": "spring boot",
        "body": "wellcome",
        "userName": "홍길동1",
        "createdAt": "2022-12-27T06:54:24.214842",
        "lastModifiedAt": "2022-12-27T06:55:32.224082"
      }
    }
    ```

    - 실패 (포스트가 존재하지 않는 경우)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "해당 포스트가 없습니다.",
        "errorCode": "POST_NOT_FOUND"
      }
    }
    ```


### 5️⃣ 포스트 수정 : Put /api/v1/posts

- 포스트 수정 성공 시 `message`, `postId` 를 반환한다.
- 실행 결과
    - 성공

    ```json
    {
      "resultCode": "SUCCESS",
      "result": {
        "message": "포스트 수정 완료",
        "postId": 20
      }
    }
    ```

    - 실패 (자신의 포스트가 아닌 경우)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "사용자가 권한이 없습니다.",
        "errorCode": "INVALID_PERMISSION"
      }
    }
    ```

    - 실패 (토큰 인증 실패한 경우)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "잘못된 토큰입니다.",
        "errorCode": "UNAUTHORIZED"
      }
    }
    ```


### 6️⃣ 포스트 삭제 : Delete /api/v1/posts

- 포스트 삭제 성공 시 `message` , `postId`  를 반환한다.
- 실행결과
    - 성공

    ```json
    {
      "resultCode": "SUCCESS",
      "result": {
        "message": "포스트 삭제 완료",
        "postId": 22
      }
    }
    ```

    - 실패 (자신의 포스트가 아닌 경우)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "사용자가 권한이 없습니다.",
        "errorCode": "INVALID_PERMISSION"
      }
    }
    ```

    - 실패 (토큰 인증 실패한 경우)

    ```json
    {
      "resultCode": "ERROR",
      "result": {
        "message": "잘못된 토큰입니다.",
        "errorCode": "UNAUTHORIZED"
      }
    }
    ```


### 7️⃣ 포스트 목록 조회 : Get /api/v1/posts

- 포스트 목록 조회 성공 시 모든 포스트를 pageable 하여 반환한다.
- 실행 결과
    - 성공

    ```json
    {
      "resultCode": "SUCCESS",
      "result": {
        "content": [
          {
            "id": 21,
            "title": "hello-title",
            "body": "hello-body",
            "userName": "kyeongrok22",
            "createdAt": "2022-12-27T07:58:00.44822",
            "lastModifiedAt": "2022-12-27T07:58:00.44822"
          },
          {
            "id": 20,
            "title": "spring boot2",
            "body": "wellcome",
            "userName": "홍길동2",
            "createdAt": "2022-12-27T07:48:27.84908",
            "lastModifiedAt": "2022-12-27T07:56:04.8232"
          }
    ],
        "pageable": {
          "sort": {
            "empty": false,
            "sorted": true,
            "unsorted": false
          },
          "offset": 0,
          "pageNumber": 0,
          "pageSize": 20,
          "paged": true,
          "unpaged": false
        },
        "totalPages": 1,
        "totalElements": 20,
        "last": true,
        "size": 20,
        "number": 0,
        "sort": {
          "empty": false,
          "sorted": true,
          "unsorted": false
        },
        "numberOfElements": 20,
        "first": true,
        "empty": false
      }
    }
    ```


## 📖 회고록

1️⃣ [https://velog.io/@goodjav/1220-SNS-제작-스웨거-CICD](https://velog.io/@goodjav/1220-SNS-%EC%A0%9C%EC%9E%91-%EC%8A%A4%EC%9B%A8%EA%B1%B0-CICD)

2️⃣ [https://velog.io/@goodjav/1220-SNS-제작-회원가입](https://velog.io/@goodjav/1220-SNS-%EC%A0%9C%EC%9E%91-%ED%9A%8C%EC%9B%90%EA%B0%80%EC%9E%85)

3️⃣ [https://velog.io/@goodjav/1221-SNS-제작-로그인](https://velog.io/@goodjav/1221-SNS-%EC%A0%9C%EC%9E%91-%EB%A1%9C%EA%B7%B8%EC%9D%B8)

4️⃣ [https://velog.io/@goodjav/1226-SNS-제작-포스트-작성](https://velog.io/@goodjav/1226-SNS-%EC%A0%9C%EC%9E%91-%ED%8F%AC%EC%8A%A4%ED%8A%B8-%EC%9E%91%EC%84%B1)

5️⃣ [https://velog.io/@goodjav/1227-SNS-제작-포스트-상세-조회-포스트-수정](https://velog.io/@goodjav/1227-SNS-%EC%A0%9C%EC%9E%91-%ED%8F%AC%EC%8A%A4%ED%8A%B8-%EC%83%81%EC%84%B8-%EC%A1%B0%ED%9A%8C-%ED%8F%AC%EC%8A%A4%ED%8A%B8-%EC%88%98%EC%A0%95)

## 🔧 리팩토링 사항

- [x]  엔티티 클래스에서 setter 제거하기
- [x]  Post toResponse 메서드 이동하기
- [x]  @JsonFormat 사용해서 PostGetResponse 시간필드 수정하기
- [x]  컨트롤러 메소드 명과 서비스 메소드 명을 동일하게 수정하기
- [x]  NotNull 어노테이션 사용하기
- [ ]  Spring Security 테스트 추가하기
- [ ]  스웨거 수정하기
- [x]  리드미 수정하기