package com.example.project_like_lion_sns.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelloServiceTest {

    HelloService helloService = new HelloService();

    @Test
    void name() {

        Integer ans = helloService.sumOfDigit(1234);

        assertEquals(ans,10);
    }
}