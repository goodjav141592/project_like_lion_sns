package com.example.project_like_lion_sns.exception.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ErrorResponse {
    private String ErrorCode;
    private String message;

    public static ErrorResponse of(String errorCode, String message) {
        return ErrorResponse.builder()
                .ErrorCode(errorCode)
                .message(message)
                .build();
    }
}
