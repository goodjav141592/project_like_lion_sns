package com.example.project_like_lion_sns.repository;

import com.example.project_like_lion_sns.domain.entity.Post;
import com.example.project_like_lion_sns.domain.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post,Long> {
    Page<Post> findByUser(Pageable pageable, User user);
}
