package com.example.project_like_lion_sns.service;

import org.springframework.stereotype.Service;

@Service
public class HelloService {
    public Integer sumOfDigit(Integer num) {
        int ans = 0;
        while (num > 0) {
            ans += num % 10;
            num /= 10;
        }
        return ans;
    }
}
