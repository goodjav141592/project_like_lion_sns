package com.example.project_like_lion_sns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ProjectLikeLionSnsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectLikeLionSnsApplication.class, args);
    }

}
