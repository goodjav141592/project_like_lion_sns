package com.example.project_like_lion_sns.controller;

import com.example.project_like_lion_sns.domain.dto.Response;
import com.example.project_like_lion_sns.domain.dto.alarm.AlarmResponse;
import com.example.project_like_lion_sns.service.AlarmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/alarms")
@RequiredArgsConstructor
@Api(tags = "Alarm API")
public class AlarmController {

    private final AlarmService alarmService;

    @GetMapping
    @ApiOperation(value = "알람 조회")
    public ResponseEntity<Response> getAlarms(@PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) @ApiIgnore Pageable pageable, @ApiIgnore Authentication authentication) {
        String userName = authentication.getName();
        Page<AlarmResponse> alarmResponses = alarmService.getAlarms(pageable, userName);
        return ResponseEntity.ok().body(Response.of("SUCCESS", alarmResponses));
    }
}
