package com.example.project_like_lion_sns.controller;

import com.example.project_like_lion_sns.service.HelloService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/hello")
@RequiredArgsConstructor
@ApiIgnore
public class HelloController {

    private final HelloService helloService;

    @GetMapping("")
    public ResponseEntity<String> hello(){
        return ResponseEntity.ok().body("김지수");
    }

    @GetMapping("/{num}")
    public ResponseEntity<Integer> sumOfDigit(@PathVariable Integer num){
        Integer result = helloService.sumOfDigit(num);
        return ResponseEntity.ok().body(result);
    }
}
