package com.example.project_like_lion_sns.domain.dto.join;

import lombok.*;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UserJoinRequest {
    @NotBlank(message = "UserName을 입력해 주세요.")
    private String userName;
    @NotBlank(message = "패스워드를 입력해 주세요.")
    private String password;
}
