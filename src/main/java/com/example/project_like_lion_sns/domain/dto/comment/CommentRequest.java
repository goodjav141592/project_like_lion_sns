package com.example.project_like_lion_sns.domain.dto.comment;

import lombok.*;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CommentRequest {
    @NotBlank(message = "댓글을 입력해 주세요.")
    private String comment;
}
