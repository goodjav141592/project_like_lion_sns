package com.example.project_like_lion_sns.domain.dto.login;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserLoginResponse {
    private String jwt;

    public static UserLoginResponse of(String token){
        return UserLoginResponse.builder()
                .jwt(token)
                .build();
    }
}
