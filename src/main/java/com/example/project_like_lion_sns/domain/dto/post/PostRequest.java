package com.example.project_like_lion_sns.domain.dto.post;

import lombok.*;

import javax.validation.constraints.NotBlank;
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class PostRequest {
    @NotBlank(message = "제목을 입력해 주세요.")
    private String title;
    @NotBlank(message = "내용을 입력해 주세요.")
    private String body;
}

